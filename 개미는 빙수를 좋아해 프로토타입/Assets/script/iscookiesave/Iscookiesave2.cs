﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Iscookiesave2 : MonoBehaviour {
    public bool iscookiesave1, iscookiesave2, iscookiesave3;
    private GameManager gamemanager;
    private void Awake()
    {
        iscookiesave1 = false;
        iscookiesave2 = false;
        iscookiesave3 = false;
    }

    public void ics()
    {
        if (iscookiesave1 == true)
        {
            GameObject.Find("GameManager").GetComponent<GameManager>().Co121();
            GameObject.Find("GameManager").GetComponent<GameManager>().Cookie2();
        }

        if (iscookiesave2 == true)
        {
            GameObject.Find("GameManager").GetComponent<GameManager>().Co122();
            GameObject.Find("GameManager").GetComponent<GameManager>().Cookie2();
        }

        if (iscookiesave3 == true)
        {
            GameObject.Find("GameManager").GetComponent<GameManager>().Co123();
            GameObject.Find("GameManager").GetComponent<GameManager>().Cookie2();
        }
    }
    public void Iscs4()
    {
        iscookiesave1 = true;
    }

    public void Iscs5()
    {
        iscookiesave2 = true;
    }

    public void Iscs6()
    {
        iscookiesave3 = true;
    }


}
