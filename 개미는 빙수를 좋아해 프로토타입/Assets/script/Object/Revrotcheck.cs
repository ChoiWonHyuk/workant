﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Revrotcheck : MonoBehaviour
{


    private void OnTriggerStay(Collider col)
    {

        if (col.transform.tag == "ground")
        {

            Antmove.AS.revrotate = true;
            //Debug.Log("rotfalse");

        }

    }
    private void OnTriggerEnter(Collider col)
    {
        if (Antmove.AS.waiting == true)
        {
            if (col.transform.tag == "wall")
                Antmove.AS.reverse++;

            if (Antmove.AS.reverse % 2 == 0)//스케일값
                Antmove.AS.transform.localScale = Vector3.Scale(transform.localScale, new Vector3(-6, 6, 30));
            else if (Antmove.AS.reverse % 2 == 1)
                Antmove.AS.transform.localScale = Vector3.Scale(transform.localScale, new Vector3(6, 6, 30));

        }
    }

    private void OnTriggerExit(Collider col)
    {
        if (col.transform.tag == "ground")
        {
            Antmove.AS.revrotate = false;
            //Debug.Log("rotrue");
        }
    }
}
