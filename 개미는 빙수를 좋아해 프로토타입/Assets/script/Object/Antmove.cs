﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Antmove : MonoBehaviour
{

    public static Antmove AS;
    Rigidbody rigid;
    GameObject other;
    public bool rotate = false;
    public bool revrotate = false;
    public bool Forcestrong = true;
    public bool waiting = true;
    public int reverse = 0;
    public UIEvent UIEvent;
    private CookieManager CookieManager;
    public Slider healthBarSlider;
    public static bool isPaused;
    public static Vector3 pos;
    public static int score;
    private GameManager gamemanager;
    double spiketimer;
    int spiketimedelay;
    public bool move = true;
    public bool slow = false;

    void Start()
    {

        rigid = GetComponent<Rigidbody>();
        Time.timeScale = 8f;
        AS = this;


        rigid.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezePositionZ;
        score = 0;

        spiketimer = spiketimedelay;
        spiketimedelay = 10;
    }

    



    void Update()
    {

        if (move == true)
        {
            Move();
            Checkground();
            if(waiting == false)
            {
                healthBarSlider.value -= 1;
            }
            
        }
        pos = this.gameObject.transform.position;

        if (healthBarSlider.value <= 0)
        { 
            Time.timeScale = 0f;
            move = false;
            
        }
        if (spiketimer < spiketimedelay)
        {

            spiketimer += Time.deltaTime;
        }
    }


    void Move()
    {
            if (reverse % 2 == 0 && slow == false)
            {
                rigid.transform.Translate(new Vector3(1.5f, 0, 0));
                
            }
            else if (reverse % 2 == 0 && slow == true)
            {
                rigid.transform.Translate(new Vector3(1f, 0, 0));
            }
            if (reverse % 2 == 1 && slow == false)
            {
                rigid.transform.Translate(new Vector3(-1.5f, 0, 0));
            }
            else if (reverse % 2 == 1 && slow == true)
            {
                rigid.transform.Translate(new Vector3(-1f, 0, 0));
            }




        if (rotate == true)
            {
                if (reverse % 2 == 0)
                {
                    transform.Rotate(0, 0, -2);
                }
                if (reverse % 2 == 1)
                {
                    transform.Rotate(0, 0, 2);
                }
            }

            else if (revrotate == true)
            {
                if (reverse % 2 == 0)
                {
                    transform.Rotate(0, 0, 8);
                }
                if (reverse % 2 == 1)
                {
                    transform.Rotate(0, 0, -8);
                }

                Forcestrong = false;
            }
    }




    void Checkground()
    {
            RaycastHit hit;
            Debug.DrawRay(transform.position, -transform.up, Color.blue, 10f);


            //Debug.Log(hit.collider.name);
            if (Physics.Raycast(transform.position, -transform.up, out hit))
            {
                // Debug.Log(hit.transform.name);
                Force();
            }
    }
    void Force()
    {
            if (Forcestrong == true)
            {
                rigid.AddRelativeForce(-Vector3.up * 3000);
            }
            else
                rigid.AddRelativeForce(-Vector3.up * 2000);
    }


    private void OnTriggerExit(Collider col)
    {
        if (col.transform.tag == "start")
        {
            waiting = false;           
        }

        if(col.transform.tag == "web")
        {
            slow = false;
        }
    }

    private void OnTriggerStay(Collider col)
    {
        if(col.transform.tag == "web")
        {
            slow = true;
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "frog")
        {
            Destroy(gameObject);
            healthBarSlider.value = 0;
        }
        if (col.gameObject.tag == "frogdie")
        {
            transform.Rotate(0, 0, 1);
            
        }
        if (col.gameObject.tag == "spike" && spiketimer > spiketimedelay && healthBarSlider.value > 0)
        {
            healthBarSlider.value -= 30;
            spiketimer = 0;
        }
        if (col.gameObject.tag == "NextLevel")
        {
            LevelControl.instance.Youwin();
            move = false;
            Time.timeScale = 0f; 
        }
        if (col.gameObject.tag == "cookie111")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie1();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave1>().Iscs1();
        }
        if (col.gameObject.tag == "cookie112")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie2();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave1>().Iscs2();
        }
        if (col.gameObject.tag == "cookie113")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie3();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave1>().Iscs3();
        }

        if (col.gameObject.tag == "cookie121")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie1();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave2>().Iscs4();
        }
        if (col.gameObject.tag == "cookie122")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie2();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave2>().Iscs5();
        }
        if (col.gameObject.tag == "cookie123")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie3();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave2>().Iscs6();
        }

        if (col.gameObject.tag == "cookie131")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie1();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave3>().Iscs7();
        }
        if (col.gameObject.tag == "cookie132")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie2();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave3>().Iscs8();
        }
        if (col.gameObject.tag == "cookie133")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie3();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave3>().Iscs9();
        }

        if (col.gameObject.tag == "cookie211")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie1();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave4>().Iscs10();
        }
        if (col.gameObject.tag == "cookie212")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie2();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave4>().Iscs11();
        }
        if (col.gameObject.tag == "cookie213")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie3();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave4>().Iscs12();
        }

        if (col.gameObject.tag == "cookie221")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie1();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave5>().Iscs13();
        }
        if (col.gameObject.tag == "cookie222")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie2();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave5>().Iscs14();
        }
        if (col.gameObject.tag == "cookie223")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie3();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave5>().Iscs15();
        }

        if (col.gameObject.tag == "cookie231")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie1();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave6>().Iscs16();
        }
        if (col.gameObject.tag == "cookie232")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie2();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave6>().Iscs17();
        }
        if (col.gameObject.tag == "cookie233")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie3();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave6>().Iscs18();
        }
        if (col.gameObject.tag == "cookie311")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie1();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave7>().Iscs19();
        }
        if (col.gameObject.tag == "cookie312")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie2();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave7>().Iscs20();
        }
        if (col.gameObject.tag == "cookie313")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie3();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave7>().Iscs21();
        }

        if (col.gameObject.tag == "cookie321")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie1();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave8>().Iscs22();
        }
        if (col.gameObject.tag == "cookie322")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie2();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave8>().Iscs23();
        }
        if (col.gameObject.tag == "cookie323")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie3();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave8>().Iscs24();
        }

        if (col.gameObject.tag == "cookie331")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie1();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave9>().Iscs25();
        }
        if (col.gameObject.tag == "cookie332")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie2();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave9>().Iscs26();
        }
        if (col.gameObject.tag == "cookie333")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie3();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave9>().Iscs27();
        }
        if (col.gameObject.tag == "cookie411")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie1();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave10>().Iscs28();
        }
        if (col.gameObject.tag == "cookie412")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie2();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave10>().Iscs29();
        }
        if (col.gameObject.tag == "cookie413")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie3();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave10>().Iscs30();
        }

        if (col.gameObject.tag == "cookie421")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie1();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave11>().Iscs31();
        }
        if (col.gameObject.tag == "cookie422")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie2();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave11>().Iscs32();
        }
        if (col.gameObject.tag == "cookie423")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie3();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave11>().Iscs33();
        }

        if (col.gameObject.tag == "cookie431")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie1();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave12>().Iscs34();
        }
        if (col.gameObject.tag == "cookie432")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie2();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave12>().Iscs35();
        }
        if (col.gameObject.tag == "cookie433")
        {
            Destroy(col.gameObject, 0f);
            GameObject.Find("CookieManager").GetComponent<CookieManager>().Cookie3();
            GameObject.Find("iscookiesave").GetComponent<Iscookiesave12>().Iscs36();
        }



    }
}