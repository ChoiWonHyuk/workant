﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour {

    public GameObject Fire_Ant;

    private Vector3 offset;

    Transform AT;

	// Use this for initialization
	void Start () {
        AT = Fire_Ant.transform;
    }
	
	// Update is called once per frame
	void Update () {
        transform.position = Vector3.Lerp(transform.position, AT.position, 1f * Time.deltaTime);
        transform.Translate(0, 0, -10);
    }
}
