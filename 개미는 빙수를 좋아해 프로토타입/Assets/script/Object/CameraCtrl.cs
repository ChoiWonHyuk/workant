﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCtrl : MonoBehaviour {

    public static int campos = 0;
    public GameObject Target1;
    public GameObject Target2;
    public GameObject Target3;
    public GameObject Target4;
    public float CameraZ = -10;
    public static Vector3 pos;



    public void Update()
    {
        pos = this.gameObject.transform.position;
    }

    // Update is called once per frame
    void FixedUpdate () {
        //A.transform.position = Vector3.Lerp(A.transform.position, pos, Time.deltaTime);
        if(Antmove.pos.y > 150)
        {
            campos = 0;
        }

        if(campos == 0)
        {
            transform.position = new Vector3(102, 95, -10);
        }
        else if (campos == 1)
        {   
             Vector3 TargetPos = new Vector3(Target1.transform.position.x, Target1.transform.position.y, CameraZ);
             transform.position = Vector3.Lerp(transform.position, TargetPos, Time.deltaTime * 0.1f);
        }
        else if(campos == 2)
        {
            Vector3 TargetPos = new Vector3(Target2.transform.position.x, Target2.transform.position.y, CameraZ);
            transform.position = Vector3.Lerp(transform.position, TargetPos, Time.deltaTime * 0.1f);

        }
        else if(campos == 3)
        {
            Vector3 TargetPos = new Vector3(Target3.transform.position.x, Target3.transform.position.y, CameraZ);
            transform.position = Vector3.Lerp(transform.position, TargetPos, Time.deltaTime * 0.1f);
        }
        else if (campos == 4)
        {
            Vector3 TargetPos = new Vector3(Target4.transform.position.x, Target4.transform.position.y, CameraZ);
            transform.position = Vector3.Lerp(transform.position, TargetPos, Time.deltaTime * 0.1f);
        }

    }
}
