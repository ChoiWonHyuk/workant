﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotcheck : MonoBehaviour {


    private void OnTriggerStay(Collider col)
    {

        if (col.transform.tag == "ground")
        {

            Antmove.AS.rotate = false;
            //Debug.Log("rotfalse");

        }
    }

    private void OnTriggerExit(Collider col)
    {
        if (col.transform.tag == "ground")
        {
            Antmove.AS.rotate = true;
            Antmove.AS.Forcestrong = true;
            //Debug.Log("rottrue");
        }
    }


}
