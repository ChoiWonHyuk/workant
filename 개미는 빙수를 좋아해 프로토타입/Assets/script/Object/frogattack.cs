﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class frogattack : MonoBehaviour {

    Animator animator;
   

    private int colcheck = 0;

    bool antin;
    void Awake()
    {
        
        animator = GetComponent<Animator>();
    }

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "ant")
        {
            colcheck = 1;
            //Debug.Log("colcheck on");
        }
        else
        {
            colcheck = 0;
            //Debug.Log("colcheck off");
        }
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        AnimationUpdate();
	}

    void AnimationUpdate()
    {
        if(colcheck == 1)
        {
            animator.SetBool("antin", true);
        }
        else if(colcheck == 0)
        {
            animator.SetBool("antin", false);
        }
    }
}
