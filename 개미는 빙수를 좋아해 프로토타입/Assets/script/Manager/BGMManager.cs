﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//https://medium.com/wasd/unity2d-9-slider를-이용한-볼륨조절-c88ec75f752d

public class BGMManager : MonoBehaviour {

    public Slider backVolume;
    public new AudioSource audio;


    private float backVol = 1f;

    // Use this for initialization
    private void Start()
    {

        backVol = PlayerPrefs.GetFloat("backvol", 1f);
        backVolume.value = backVol;
        audio.volume = backVolume.value;



    }

    // Update is called once per frame
    void Update()
    {

        SoundSlider();
    }

    public void SoundSlider()
    {

        audio.volume = backVolume.value;

        backVol = backVolume.value;
        PlayerPrefs.SetFloat("backvol", backVol);
    }
}
