﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CookieManager : MonoBehaviour
{
    public Image Cookiefront1;
    public Image Cookiefront2;
    public Image Cookiefront3;
    public Image ClearCookie1;
    public Image ClearCookie2;
    public Image ClearCookie3;

    public void Start()
    {
        Cookiefront1.enabled = false;
        Cookiefront2.enabled = false;
        Cookiefront3.enabled = false;

        ClearCookie1.enabled = false;
        ClearCookie2.enabled = false;
        ClearCookie3.enabled = false;
    }
    public void Cookie1()
    {
        Debug.Log("c1");
        Cookiefront1.enabled = true;
        ClearCookie1.enabled = true;
    }

    public void Cookie2()
    {
        Debug.Log("c2");
        Cookiefront2.enabled = true;
        ClearCookie2.enabled = true;
    }

    public void Cookie3()
    {
        Debug.Log("c3");
        Cookiefront3.enabled = true;
        ClearCookie3.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {

    }
}