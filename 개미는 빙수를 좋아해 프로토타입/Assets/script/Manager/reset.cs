﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class reset : MonoBehaviour {
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    static void levelreset()
    {
        PlayerPrefs.SetInt("LevelPassed", 0);
    }

}
