﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
//스테이지별 쿠키 획득 수 저장용, esc로 종료하는 용도
public class GameManager : MonoBehaviour {
    public int co111, co112, co113, co121, co122, co123, co131, co132, co133, co211, co212,co213,co221,co222,co223,co231,co232,co233,co311,co312,co313,co321,co322,co323,co331,co332,co333,co411,co412,co413,co421,co422,co423,co431,co432,co433 = 0;
    public int cookie1, cookie2, cookie3, cookie4, cookie5, cookie6, cookie7, cookie8, cookie9, cookie10, cookie11, cookie12, collectedcookie = 0;
    public static GameManager instance = null;
    public void Awake()
    {
        Screen.SetResolution(1920, 1200, true);

        if (instance == null)
            instance = this;

        else if (instance != null)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }
    public void Cookie1()
    {
        if (cookie1 < 3)
        cookie1++;
    }
    public void Cookie2()
    {
        if (cookie2 < 3)
            cookie2++;
    }
    public void Cookie3()
    {
        if (cookie3 < 3)
            cookie3++;
    }
    public void Cookie4()
    {
        if (cookie4 < 3)
            cookie4++;
    }
    public void Cookie5()
    {
        if (cookie5 < 3)
            cookie5++;
    }
    public void Cookie6()
    {
        if (cookie6 < 3)
            cookie6++;
    }
    public void Cookie7()
    {
        if (cookie7 < 3)
            cookie7++;
    }
    public void Cookie8()
    {
        if (cookie8 < 3)
            cookie8++;
    }
    public void Cookie9()
    {
        if (cookie9 < 3)
            cookie9++;
    }
    public void Cookie10()
    {
        if (cookie10 < 3)
            cookie10++;
    }
    public void Cookie11()
    {
        if (cookie11 < 3)
            cookie11++;
    }
    public void Cookie12()
    {
        if (cookie12 < 3)
            cookie12++;
    }
    public void EdC()
    {
        if (collectedcookie >= 4)
        {
            SceneManager.LoadScene("Ending_First");
        }
    }



    public void Co111()
    {
        co111 = 1;
    }
    public void Co112()
    {
        co112 = 1;
    }
    public void Co113()
    {
        co113 = 1;
    }
    public void Co121()
    {
        co121 = 1;
    }
    public void Co122()
    {
        co122 = 1;
    }
    public void Co123()
    {
        co123 = 1;
    }
    public void Co131()
    {
        co131 = 1;
    }
    public void Co132()
    {
        co132 = 1;
    }
    public void Co133()
    {
        co133 = 1;
    }
    public void Co211()
    {
        co211 = 1;
    }
    public void Co212()
    {
        co212 = 1;
    }
    public void Co213()
    {
        co213 = 1;
    }
    public void Co221()
    {
        co221 = 1;
    }
    public void Co222()
    {
        co222 = 1;
    }
    public void Co223()
    {
        co223 = 1;
    }
    public void Co231()
    {
        co231 = 1;
    }
    public void Co232()
    {
        co232 = 1;
    }
    public void Co233()
    {
        co233 = 1;
    }
    public void Co311()
    {
        co311 = 1;
    }
    public void Co312()
    {
        co312 = 1;
    }
    public void Co313()
    {
        co313 = 1;
    }
    public void Co321()
    {
        co321 = 1;
    }
    public void Co322()
    {
        co322 = 1;
    }
    public void Co323()
    {
        co323 = 1;
    }
    public void Co331()
    {
        co331 = 1;
    }
    public void Co332()
    {
        co332 = 1;
    }
    public void Co333()
    {
        co333 = 1;
    }
    public void Co411()
    {
        co411 = 1;
    }
    public void Co412()
    {
        co412 = 1;
    }
    public void Co413()
    {
        co413 = 1;
    }
    public void Co421()
    {
        co421 = 1;
    }
    public void Co422()
    {
        co422 = 1;
    }
    public void Co423()
    {
        co423 = 1;
    }
    public void Co431()
    {
        co431 = 1;
    }
    public void Co432()
    {
        co432 = 1;
    }
    public void Co433()
    {
        co433 = 1;
    }




    void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        collectedcookie = cookie1 + cookie2 + cookie3 + cookie4 + cookie5 + cookie6 + cookie7 + cookie8 + cookie9 + cookie10 + cookie11 + cookie12;
        if (co111 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff1>().CoO111();
        }

        if (co112 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff1>().CoO112();
        }

        if (co113 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff1>().CoO113();
        }

        if (co121 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff2>().CoO121();
        }

        if (co122 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff2>().CoO122();
        }

        if (co123 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff2>().CoO123();
        }

        if (co131 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff3>().CoO131();
        }

        if (co132 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff3>().CoO132();
        }

        if (co133 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff3>().CoO133();
        }
        if (co211 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff4>().CoO211();
        }

        if (co212 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff4>().CoO212();
        }

        if (co213 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff4>().CoO213();
        }

        if (co221 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff5>().CoO221();
        }

        if (co222 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff5>().CoO222();
        }

        if (co223 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff5>().CoO223();
        }

        if (co231 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff6>().CoO231();
        }

        if (co232 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff6>().CoO232();
        }

        if (co233 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff6>().CoO233();
        }
        if (co311 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff7>().CoO311();
        }

        if (co312 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff7>().CoO312();
        }

        if (co313 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff7>().CoO313();
        }

        if (co321 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff8>().CoO321();
        }

        if (co322 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff8>().CoO322();
        }

        if (co323 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff8>().CoO323();
        }

        if (co331 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff9>().CoO331();
        }

        if (co332 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff9>().CoO332();
        }

        if (co333 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff9>().CoO333();
        }
        if (co411 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff10>().CoO411();
        }

        if (co412 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff10>().CoO412();
        }

        if (co413 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff10>().CoO413();
        }

        if (co421 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff11>().CoO421();
        }

        if (co422 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff11>().CoO422();
        }

        if (co423 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff11>().CoO423();
        }

        if (co431 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff12>().CoO431();
        }

        if (co432 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff12>().CoO432();
        }

        if (co433 == 1)
        {
            GameObject.Find("cookiesave").GetComponent<Cookieonoff12>().CoO433();
        }
    }   
}
