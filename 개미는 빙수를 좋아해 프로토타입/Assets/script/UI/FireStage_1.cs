﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FireStage_1 : MonoBehaviour
{
    public int index = 0;
    public GameObject FireStageMenu_1;
    public bool limitTouch = false;
    public static FireStage_1 FS;

    private void Start()
    {
        FireStageMenu_1.SetActive(false);
    }

    public void Fire_1()
    {

        Debug.Log("STAGE1");

        if (Input.GetMouseButtonUp(0))
        {
            FireStageMenu_1.SetActive(true);
            limitTouch = true;
        }
        
    }
}
