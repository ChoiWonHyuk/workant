﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainStart : MonoBehaviour {

    public string Start;

    public void Main()
    {
        if (Input.GetMouseButtonUp(0))
        {

            SceneManager.LoadScene(Start);

        }
    }
}
