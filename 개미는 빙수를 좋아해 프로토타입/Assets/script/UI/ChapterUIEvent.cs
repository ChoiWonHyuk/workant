﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChapterUIEvent : MonoBehaviour
{

    public static bool SettingOn;
    private GameObject SettingUI;
    private GameObject Help;
    private GameObject SoundSetting;
    private GameObject NormalPanel;
    private GameObject NextPage1;
    private GameObject NextPage2;
    private GameObject NextPage3;

    void Start()
    {
        Time.timeScale = 1f;
        SettingOn = false;
        SettingUI.SetActive(false);


    }

    void Awake()
    {
        NormalPanel = GameObject.Find("MasterCanvas").transform.Find("SettingButton").gameObject;
        SettingUI = GameObject.Find("MasterCanvas").transform.Find("SettingUI").gameObject;
        Help = GameObject.Find("MasterCanvas").transform.Find("HelpUI").gameObject;
        SoundSetting = GameObject.Find("MasterCanvas").transform.Find("SoundsettingUI").gameObject;
        NextPage1 = GameObject.Find("MasterCanvas").transform.Find("HelpUI").transform.Find("Panel").gameObject;
        NextPage2 = GameObject.Find("MasterCanvas").transform.Find("HelpUI").transform.Find("Panel (1)").gameObject;
        NextPage3 = GameObject.Find("MasterCanvas").transform.Find("HelpUI").transform.Find("Panel (2)").gameObject;
    }

    public void ActiveSettingButton()
    {
        if (!SettingOn)
        {
            SettingUI.SetActive(true);
            NormalPanel.SetActive(true);

        }
        else
        {
            SettingUI.SetActive(false);
            NormalPanel.SetActive(true);
        }
        SettingOn = !SettingOn;
    }

    public void SettingBackButton()
    {
        SettingUI.SetActive(false);
    }

    public void HelpButton()
    {
        Help.SetActive(true);
    }

    public void NextButton1()
    {
        NextPage1.SetActive(false);
    }

    public void BackButton1()
    {
        NextPage1.SetActive(true);
    }

    public void BackButton2()
    {
        NextPage2.SetActive(true);
    }

    public void NextButton2()
    {
        NextPage2.SetActive(false);
    }


    public void HelpBackButton()
    {
        Help.SetActive(false);
    }

    public void SoundSettingButton()
    {
        SoundSetting.SetActive(true);
    }

    public void SoundSettingBackButton()
    {
        SoundSetting.SetActive(false);
    }

    public void Quit()
    {
        Application.Quit();
    }
}

