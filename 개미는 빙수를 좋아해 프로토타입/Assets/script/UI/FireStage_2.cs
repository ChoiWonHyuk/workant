﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FireStage_2 : MonoBehaviour
{
   
    public GameObject FireStageMenu_2;

    private void Start()
    {
        FireStageMenu_2.SetActive(false);
    }

    public void Fire_2()
    {

        Debug.Log("STAGE2");

        if (Input.GetMouseButtonUp(0))
        {
            FireStageMenu_2.SetActive(true);
        }
    }
}
