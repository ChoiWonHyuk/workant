﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UnlockStage : MonoBehaviour {

    public Button Fire2, Fire3, Desert1, Desert2, Desert3, Plants1, Plants2, Plants3, Ice1, Ice2, Ice3;
    int levelPassed;

    private void Start()
    {
        levelPassed = PlayerPrefs.GetInt("LevelPassed");
        Fire2.interactable = false;
        Fire3.interactable = false;
        Desert1.interactable = false;
        Desert2.interactable = false;
        Desert3.interactable = false;
        Plants1.interactable = false;
        Plants2.interactable = false;
        Plants3.interactable = false;
        Ice1.interactable = false;
        Ice2.interactable = false;
        Ice3.interactable = false;

        switch (levelPassed)
        {
            case 2:
                Fire2.interactable = true;
                break;
            case 3:
                Fire2.interactable = true;
                Fire3.interactable = true;
                break;
            case 4:
                Fire2.interactable = true;
                Fire3.interactable = true;
                Desert1.interactable = true;
                break;
            case 5:
                Fire2.interactable = true;
                Fire3.interactable = true;
                Desert1.interactable = true;
                Desert2.interactable = true;
                break;
            case 6:
                Fire2.interactable = true;
                Fire3.interactable = true;
                Desert1.interactable = true;
                Desert2.interactable = true;
                Desert3.interactable = true;
                break;
            case 7:
                Fire2.interactable = true;
                Fire3.interactable = true;
                Desert1.interactable = true;
                Desert2.interactable = true;
                Desert3.interactable = true;
                Plants1.interactable = true;
                break;
            case 8:
                Fire2.interactable = true;
                Fire3.interactable = true;
                Desert1.interactable = true;
                Desert2.interactable = true;
                Desert3.interactable = true;
                Plants1.interactable = true;
                Plants2.interactable = true;
                break;
            case 9:
                Fire2.interactable = true;
                Fire3.interactable = true;
                Desert1.interactable = true;
                Desert2.interactable = true;
                Desert3.interactable = true;
                Plants1.interactable = true;
                Plants2.interactable = true;
                Plants3.interactable = true;
                break;
            case 10:
                Fire2.interactable = true;
                Fire3.interactable = true;
                Desert1.interactable = true;
                Desert2.interactable = true;
                Desert3.interactable = true;
                Plants1.interactable = true;
                Plants2.interactable = true;
                Plants3.interactable = true;
                Ice1.interactable = true;
                break;
            case 11:
                Fire2.interactable = true;
                Fire3.interactable = true;
                Desert1.interactable = true;
                Desert2.interactable = true;
                Desert3.interactable = true;
                Plants1.interactable = true;
                Plants2.interactable = true;
                Plants3.interactable = true;
                Ice1.interactable = true;
                Ice2.interactable = true;
                break;
            case 12:
                Fire2.interactable = true;
                Fire3.interactable = true;
                Desert1.interactable = true;
                Desert2.interactable = true;
                Desert3.interactable = true;
                Plants1.interactable = true;
                Plants2.interactable = true;
                Plants3.interactable = true;
                Ice1.interactable = true;
                Ice2.interactable = true;
                Ice3.interactable = true;
                break;

        }


    }

    public void levelToLoad(int level)
    {
        SceneManager.LoadScene(level);
    }

    
    public void resetPlayerPrefs()
    {
        Fire2.interactable = false;
        Fire3.interactable = false;
        Desert1.interactable = false;
        Desert2.interactable = false;
        Desert3.interactable = false;
        Plants1.interactable = false;
        Plants2.interactable = false;
        Plants3.interactable = false;
        Ice1.interactable = false;
        Ice2.interactable = false;
        Ice3.interactable = false;
        PlayerPrefs.DeleteAll();
    }
}
