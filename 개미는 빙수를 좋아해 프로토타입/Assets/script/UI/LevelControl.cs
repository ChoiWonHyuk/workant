﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelControl : MonoBehaviour {

    public static LevelControl instance = null;
    GameObject youWinPanel;
    int SceneIndex, levelPassed;


    void Start () {

        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        youWinPanel = GameObject.Find("NextLevel");
        youWinPanel.gameObject.SetActive(false);

        SceneIndex = SceneManager.GetActiveScene().buildIndex;
        levelPassed = PlayerPrefs.GetInt("LevelPassed");
	}
	
	public void Youwin()
    {
        if (SceneIndex == 13)
            Invoke("loadMainMenu", 1f);
        else
        {
            if (levelPassed < SceneIndex)
                PlayerPrefs.SetInt("LevelPassed", SceneIndex);
            youWinPanel.gameObject.SetActive(true);
            PlayerPrefs.SetInt("LevelPassed", SceneIndex +1);
        }
    }


}
