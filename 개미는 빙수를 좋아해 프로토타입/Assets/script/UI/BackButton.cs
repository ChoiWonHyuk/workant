﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackButton : MonoBehaviour {

    public GameObject StageSelectPauseMenu;

    private void Start()
    {
        StageSelectPauseMenu.SetActive(false);
    }

    public void Changing()
    {
        if (Input.GetMouseButtonUp(0))
        {
            StageSelectPauseMenu.SetActive(true);
        }
    }
}
