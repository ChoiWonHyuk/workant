﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackMain : MonoBehaviour {

    public string GoMain;

    public void GoSMainScreen()
    {
        if (Input.GetMouseButtonUp(0))
        {
            SceneManager.LoadScene(GoMain);
        }
    }
	
}
