﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Back : MonoBehaviour {
    public GameObject Menu;

	public void BackClick()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Menu.SetActive(false);
        }
        
    }
}
