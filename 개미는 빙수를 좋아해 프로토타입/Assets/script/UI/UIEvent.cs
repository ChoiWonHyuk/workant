﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIEvent : MonoBehaviour {
    public static bool PauseOn;
    private GameObject NormalPanel;
    private GameObject PauseUI;
    private GameObject RestartPauseUI;
    private GameObject GameclearUI;
    private GameObject GameoverUI;
    private GameObject Soundsetting;
    private CookieManager cook;
    private GameObject HelpUI;
    private GameObject Cookie;
    private GameObject NextPage1;
    private GameObject NextPage2;
    private GameObject NextPage3;
    public string thisScene;
    public string StageSelect;
    

    void Start()
    {
        Time.timeScale = 8f;
        PauseOn = false;
        PauseUI.SetActive(false);


}
 
    void Awake()
    {
        NormalPanel = GameObject.Find("MasterCanvas").transform.Find("NormalPanel").gameObject;
        PauseUI = GameObject.Find("MasterCanvas").transform.Find("PauseUI").gameObject;
        RestartPauseUI = GameObject.Find("MasterCanvas").transform.Find("RestartPauseUI").gameObject;
        GameclearUI = GameObject.Find("MasterCanvas").transform.Find("GameclearUI").gameObject;
        GameoverUI = GameObject.Find("MasterCanvas").transform.Find("GameoverUI").gameObject;
        Soundsetting = GameObject.Find("MasterCanvas").transform.Find("Soundsetting").gameObject;
        Cookie = GameObject.Find("MasterCanvas").transform.Find("Cookie").gameObject;
        HelpUI = GameObject.Find("MasterCanvas").transform.Find("HelpUI").gameObject;
        NextPage1 = GameObject.Find("MasterCanvas").transform.Find("HelpUI").transform.Find("Panel").gameObject;
        NextPage2 = GameObject.Find("MasterCanvas").transform.Find("HelpUI").transform.Find("Panel (1)").gameObject;
        NextPage3 = GameObject.Find("MasterCanvas").transform.Find("HelpUI").transform.Find("Panel (2)").gameObject;

    }
    private void Update()
    {
        if(Antmove.AS.healthBarSlider.value <= 0)
        {
            Gameover();
        }

    }

    //재시작 씬불러오기만 수정하면 될듯
    public void RestartButton()
    {
        Debug.Log("게임 재시작");
        Time.timeScale = 8f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name); //1번씬 다시 로드
        
    }

    public void ActivePauseButton()
    {   if (!PauseOn)
        {
            Time.timeScale = 0f;
            PauseUI.SetActive(true);
            NormalPanel.SetActive(false);
            RestartPauseUI.SetActive(true);
            Antmove.AS.move = false;
        }
        else
        {
            Time.timeScale = 8f;
            PauseUI.SetActive(false);
            NormalPanel.SetActive(true);
            Antmove.AS.move = true;
        }
        PauseOn = !PauseOn;
    }
   // --------------------------------------------------------
  
   // -----------------------------------------------------
    //이어하기
    public void ContinueButton()
    {
        Debug.Log("퍼즈창 닫기");
        Time.timeScale = 8f;
        PauseOn = false;
        PauseUI.SetActive(false);
        NormalPanel.SetActive(true);
        RestartPauseUI.SetActive(true);
        Antmove.AS.move = true;
    }


    //챕터화면
    public void ChapterButton()
    {
        SceneManager.LoadSceneAsync("StageSelect");
    }

    //인게임 사운드설정
    public void SoundButton()
    {
        PauseUI.SetActive(false);
        Soundsetting.SetActive(true);
        RestartPauseUI.SetActive(true);
    }

    //인게임 사운드설정
    public void Sound_BackButton()
    {
        PauseUI.SetActive(true);
        Soundsetting.SetActive(false);
        RestartPauseUI.SetActive(true);
    }

    public void HelpButton()
    {
        HelpUI.SetActive(true);
    }

    public void NextButton1()
    {
        NextPage1.SetActive(false);
    }

    public void BackButton1()
    {
        NextPage1.SetActive(true);
    }

    public void BackButton2()
    {
        NextPage2.SetActive(true);
    }

    public void NextButton2()
    {
        NextPage2.SetActive(false);
    }


    public void HelpBackButton()
    {
        HelpUI.SetActive(false);
    }


    //게임나가기
    public void QuitButton()
    {
        
        Debug.Log("게임 종료");
        Application.Quit();
    }

    //게임오버
    public void Gameover()
    {
        Debug.Log("게임오버");
        Time.timeScale = 0f;
        GameoverUI.SetActive(true);
        NormalPanel.SetActive(false);
        RestartPauseUI.SetActive(false);
        Cookie.SetActive(false);
    }

    //게임클리어
    public void Gameclear()
    {
        Time.timeScale = 0;
        GameclearUI.SetActive(true);
        NormalPanel.SetActive(false);
        RestartPauseUI.SetActive(false);

    }



    public void NextLevelButton()
    {
        //SceneManager.LoadSceneAsync("NextLevel");
    }
    
   
    
}
