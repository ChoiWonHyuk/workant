﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoMainScreen : MonoBehaviour {

    public string GoScreen;
    public GameObject Panel;

    public void GoMain()
    {
        if (Input.GetMouseButtonUp(0))
        {
            SceneManager.LoadScene(GoScreen);
            Panel.SetActive(false);
        }
    }

}
