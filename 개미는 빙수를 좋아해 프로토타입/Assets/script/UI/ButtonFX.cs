﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonFX : MonoBehaviour
{

    public AudioSource MyFX;
    public AudioClip HoverFX;
    public AudioClip ClickFX;

    public void ClickSound()
    {
        MyFX.PlayOneShot(ClickFX);
    }
}
