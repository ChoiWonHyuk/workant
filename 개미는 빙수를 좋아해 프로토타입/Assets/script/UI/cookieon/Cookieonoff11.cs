﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cookieonoff11 : MonoBehaviour
{

    public Image cookie421;
    public Image cookie422;
    public Image cookie423;
    private GameManager gamemanager;
    public bool cookieon421 = false;
    public bool cookieon422 = false;
    public bool cookieon423 = false;



    void Update()
    {
        if (cookieon421 == true)
        {
            cookie421.enabled = true;
        }
        else
        {
            cookie421.enabled = false;

        }
        if (cookieon422 == true)
        {
            cookie422.enabled = true;
        }
        else
        {
            cookie422.enabled = false;

        }
        if (cookieon423 == true)
        {
            cookie423.enabled = true;
        }
        else
        {
            cookie423.enabled = false;

        }
    }

    public void CoO421()
    {
        cookieon421 = true;
    }

    public void CoO422()
    {
        cookieon422 = true;
    }

    public void CoO423()
    {
        cookieon423 = true;
    }


}
