﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cookieonoff5 : MonoBehaviour
{

    public Image cookie221;
    public Image cookie222;
    public Image cookie223;
    private GameManager gamemanager;
    public bool cookieon221 = false;
    public bool cookieon222 = false;
    public bool cookieon223 = false;



    void Update()
    {
        if (cookieon221 == true)
        {
            cookie221.enabled = true;
        }
        else
        {
            cookie221.enabled = false;

        }
        if (cookieon222 == true)
        {
            cookie222.enabled = true;
        }
        else
        {
            cookie222.enabled = false;

        }
        if (cookieon223 == true)
        {
            cookie223.enabled = true;
        }
        else
        {
            cookie223.enabled = false;

        }
    }

    public void CoO221()
    {
        cookieon221 = true;
    }

    public void CoO222()
    {
        cookieon222 = true;
    }

    public void CoO223()
    {
        cookieon223 = true;
    }


}
