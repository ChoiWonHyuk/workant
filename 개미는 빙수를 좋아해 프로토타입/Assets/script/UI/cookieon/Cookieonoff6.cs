﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cookieonoff6 : MonoBehaviour
{

    public Image cookie231;
    public Image cookie232;
    public Image cookie233;
    private GameManager gamemanager;
    public bool cookieon231 = false;
    public bool cookieon232 = false;
    public bool cookieon233 = false;



    void Update()
    {
        if (cookieon231 == true)
        {
            cookie231.enabled = true;
        }
        else
        {
            cookie231.enabled = false;

        }
        if (cookieon232 == true)
        {
            cookie232.enabled = true;
        }
        else
        {
            cookie232.enabled = false;

        }
        if (cookieon233 == true)
        {
            cookie233.enabled = true;
        }
        else
        {
            cookie233.enabled = false;

        }
    }

    public void CoO231()
    {
        cookieon231 = true;
    }

    public void CoO232()
    {
        cookieon232 = true;
    }

    public void CoO233()
    {
        cookieon233 = true;
    }


}
