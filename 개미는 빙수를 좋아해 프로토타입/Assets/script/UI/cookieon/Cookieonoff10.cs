﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cookieonoff10 : MonoBehaviour
{

    public Image cookie411;
    public Image cookie412;
    public Image cookie413;
    private GameManager gamemanager;
    public bool cookieon411 = false;
    public bool cookieon412 = false;
    public bool cookieon413 = false;



    void Update()
    {
        if (cookieon411 == true)
        {
            cookie411.enabled = true;
        }
        else
        {
            cookie411.enabled = false;

        }
        if (cookieon412 == true)
        {
            cookie412.enabled = true;
        }
        else
        {
            cookie412.enabled = false;

        }
        if (cookieon413 == true)
        {
            cookie413.enabled = true;
        }
        else
        {
            cookie413.enabled = false;

        }
    }

    public void CoO411()
    {
        cookieon411 = true;
    }

    public void CoO412()
    {
        cookieon412 = true;
    }

    public void CoO413()
    {
        cookieon413 = true;
    }


}
