﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cookieonoff9 : MonoBehaviour
{

    public Image cookie331;
    public Image cookie332;
    public Image cookie333;
    private GameManager gamemanager;
    public bool cookieon331 = false;
    public bool cookieon332 = false;
    public bool cookieon333 = false;



    void Update()
    {
        if (cookieon331 == true)
        {
            cookie331.enabled = true;
        }
        else
        {
            cookie331.enabled = false;

        }
        if (cookieon332 == true)
        {
            cookie332.enabled = true;
        }
        else
        {
            cookie332.enabled = false;

        }
        if (cookieon333 == true)
        {
            cookie333.enabled = true;
        }
        else
        {
            cookie333.enabled = false;

        }
    }

    public void CoO331()
    {
        cookieon331 = true;
    }

    public void CoO332()
    {
        cookieon332 = true;
    }

    public void CoO333()
    {
        cookieon333 = true;
    }


}
