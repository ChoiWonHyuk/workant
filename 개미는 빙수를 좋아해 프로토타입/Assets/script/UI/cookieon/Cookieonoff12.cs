﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cookieonoff12 : MonoBehaviour
{

    public Image cookie431;
    public Image cookie432;
    public Image cookie433;
    private GameManager gamemanager;
    public bool cookieon431 = false;
    public bool cookieon432 = false;
    public bool cookieon433 = false;



    void Update()
    {
        if (cookieon431 == true)
        {
            cookie431.enabled = true;
        }
        else
        {
            cookie431.enabled = false;

        }
        if (cookieon432 == true)
        {
            cookie432.enabled = true;
        }
        else
        {
            cookie432.enabled = false;

        }
        if (cookieon433 == true)
        {
            cookie433.enabled = true;
        }
        else
        {
            cookie433.enabled = false;

        }
    }

    public void CoO431()
    {
        cookieon431 = true;
    }

    public void CoO432()
    {
        cookieon432 = true;
    }

    public void CoO433()
    {
        cookieon433 = true;
    }


}
