﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cookieonoff2 : MonoBehaviour
{

    public Image cookie121;
    public Image cookie122;
    public Image cookie123;
    private GameManager gamemanager;
    public bool cookieon121 = false;
    public bool cookieon122 = false;
    public bool cookieon123 = false;



    void Update()
    {
        if (cookieon121 == true)
        {
            cookie121.enabled = true;
        }
        else
        {
            cookie121.enabled = false;

        }
        if (cookieon122 == true)
        {
            cookie122.enabled = true;
        }
        else
        {
            cookie122.enabled = false;

        }
        if (cookieon123 == true)
        {
            cookie123.enabled = true;
        }
        else
        {
            cookie123.enabled = false;

        }
    }

    public void CoO121()
    {
        cookieon121 = true;
    }

    public void CoO122()
    {
        cookieon122 = true;
    }

    public void CoO123()
    {
        cookieon123 = true;
    }


}
