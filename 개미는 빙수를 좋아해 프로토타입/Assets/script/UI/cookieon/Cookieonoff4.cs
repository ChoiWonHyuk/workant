﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cookieonoff4 : MonoBehaviour
{

    public Image cookie211;
    public Image cookie212;
    public Image cookie213;
    private GameManager gamemanager;
    public bool cookieon211 = false;
    public bool cookieon212 = false;
    public bool cookieon213 = false;



    void Update()
    {
        if (cookieon211 == true)
        {
            cookie211.enabled = true;
        }
        else
        {
            cookie211.enabled = false;

        }
        if (cookieon212 == true)
        {
            cookie212.enabled = true;
        }
        else
        {
            cookie212.enabled = false;

        }
        if (cookieon213 == true)
        {
            cookie213.enabled = true;
        }
        else
        {
            cookie213.enabled = false;

        }
    }

    public void CoO211()
    {
        cookieon211 = true;
    }

    public void CoO212()
    {
        cookieon212 = true;
    }

    public void CoO213()
    {
        cookieon213 = true;
    }


}
