﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cookieonoff8 : MonoBehaviour
{

    public Image cookie321;
    public Image cookie322;
    public Image cookie323;
    private GameManager gamemanager;
    public bool cookieon321 = false;
    public bool cookieon322 = false;
    public bool cookieon323 = false;



    void Update()
    {
        if (cookieon321 == true)
        {
            cookie321.enabled = true;
        }
        else
        {
            cookie321.enabled = false;

        }
        if (cookieon322 == true)
        {
            cookie322.enabled = true;
        }
        else
        {
            cookie322.enabled = false;

        }
        if (cookieon323 == true)
        {
            cookie323.enabled = true;
        }
        else
        {
            cookie323.enabled = false;

        }
    }

    public void CoO321()
    {
        cookieon321 = true;
    }

    public void CoO322()
    {
        cookieon322 = true;
    }

    public void CoO323()
    {
        cookieon323 = true;
    }


}
