﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cookieonoff3 : MonoBehaviour
{

    public Image cookie131;
    public Image cookie132;
    public Image cookie133;
    private GameManager gamemanager;
    public bool cookieon131 = false;
    public bool cookieon132 = false;
    public bool cookieon133 = false;



    void Update()
    {
        if (cookieon131 == true)
        {
            cookie131.enabled = true;
        }
        else
        {
            cookie131.enabled = false;

        }
        if (cookieon132 == true)
        {
            cookie132.enabled = true;
        }
        else
        {
            cookie132.enabled = false;

        }
        if (cookieon133 == true)
        {
            cookie133.enabled = true;
        }
        else
        {
            cookie133.enabled = false;

        }
    }

    public void CoO131()
    {
        cookieon131 = true;
    }

    public void CoO132()
    {
        cookieon132 = true;
    }

    public void CoO133()
    {
        cookieon133 = true;
    }


}
