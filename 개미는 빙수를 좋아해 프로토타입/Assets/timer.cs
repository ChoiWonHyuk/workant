﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class timer : MonoBehaviour {
    public double movetimer;
    public string GoScreen;
    // Use this for initialization
    void Start () {
        movetimer = 0;
	}
	
	// Update is called once per frame
	void Update () {
        movetimer += 0.1;
        if (movetimer >= 12 && movetimer <= 15) 
        {
            transform.position = new Vector3(300, 0, -10);
        }
        else if (movetimer >= 24)
        {
            SceneManager.LoadScene(GoScreen);
        }
	}
}
