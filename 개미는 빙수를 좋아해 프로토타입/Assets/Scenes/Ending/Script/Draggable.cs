﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class Draggable : MonoBehaviour, IBeginDragHandler,IDragHandler,IEndDragHandler {

    public static Draggable DA;
    public bool none = false;
    public GameObject choco;
    Vector3 Coord;
    public void OnBeginDrag(PointerEventData eventData)
    {
        
    }

    public void OnDrag(PointerEventData eventData)
    {
        
        this.transform.position = Input.mousePosition;
       
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Coord = transform.position;
        if ((Coord.x < 160 || Coord.x > -182) && (Coord.y < 340 || Coord.y > 0))
        {
            Destroy(choco);
            SceneManager.LoadScene("Ending_Fourth");
        }
    }
}
