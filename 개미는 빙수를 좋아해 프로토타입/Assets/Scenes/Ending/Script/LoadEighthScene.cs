﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadEighthScene : MonoBehaviour
{


    void Start()
    {
        Invoke("loadTwice", 2);
    }

    void loadTwice()
    {
        SceneManager.LoadScene("Ending_Eighth");
    }
}

